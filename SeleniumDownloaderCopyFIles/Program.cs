﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace SeleniumDownloaderCopyFIles
{
    class Program
    {
        static string target = "";

        static bool debug = false;
        public static Stopwatch stopwatch = new Stopwatch();
        static void Main(string[] args)
        {
            try
            {
                debug = Boolean.Parse(ConfigurationManager.AppSettings["debugMode"]);
                DateTime StartTime = DateTime.Now;
                if (debug)
                {
                    target = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + ConfigurationManager.AppSettings["debugFileLocation"];
                }
                else
                {
                    target = ConfigurationManager.AppSettings["fileLocation"];
                }
                Directory.CreateDirectory(target);

                string subtarget = target + "/BT_Import_" + DateTime.Now.ToString("MMddyyyy");

                try
                {
                    ErrorLogging.log("Starting to copy files to " + subtarget);
                    stopwatch.Start();

                    new DirectoryCopy(target, subtarget, false);

                    DateTime EndTime = DateTime.Now;
                    TimeSpan span = EndTime.Subtract(StartTime);
                    ErrorLogging.errorCatch(null, "Brightree File Backup - Duration: " + span.ToString("c"), true);
                }
                catch (Exception ex)
                {
                    DateTime EndTime = DateTime.Now;
                    TimeSpan span = EndTime.Subtract(StartTime);
                    ErrorLogging.errorCatch(ex, "Brightree File Backup - Exception - After Form  " + ex.Message + " - Duration: " + span.ToString("c"), false);
                }

                try
                {
                    var subfolder = new DirectoryInfo(target).GetDirectories("BT_Import*");

                    //This is where the files older than 3 days gets deleted
                    foreach (var folder in subfolder)
                    {
                        if (DateTime.UtcNow - folder.CreationTimeUtc > TimeSpan.FromDays(3))
                        {
                            folder.Delete(true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    DateTime EndTime = DateTime.Now;
                    TimeSpan span = EndTime.Subtract(StartTime);
                    ErrorLogging.errorCatch(ex, "Brightree File Backup - Exception - After Form  " + ex.Message + " - Duration: " + span.ToString("c"), false);
                }

                stopwatch.Stop();
                ErrorLogging.log("Finished copying files to " + subtarget + string.Format("\nTime Taken: {0} seconds", stopwatch.Elapsed.TotalSeconds));
            }
            catch (Exception ex)
            {
                DateTime EndTime = DateTime.Now;
                ErrorLogging.errorCatch(ex, "A major problem occured while copying files " + ex.Message, false);
            }
        }
    }
}
