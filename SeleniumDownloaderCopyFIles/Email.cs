﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Text;

namespace SeleniumDownloaderCopyFIles
{
    class Email
    {

        private static string email = "developers@medbill.com";
        //private static string email = "mason.moore@medbill.com";

        public static void SendEmailAsync(Exception ex, bool debug)
        {
            try
            {
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += "<br/>";
                message += "-----------------------------------------------------------";
                message += "<br/>";
                message += string.Format("Message: {0}", !string.IsNullOrEmpty(ex.Message) ? ex.Message : "");
                message += "<br/>";
                message += string.Format("StackTrace: {0}", !string.IsNullOrEmpty(ex.StackTrace) ? ex.StackTrace : "");
                message += "<br/>";
                message += string.Format("Source: {0}", !string.IsNullOrEmpty(ex.Source) ? ex.Source : "");
                message += "<br/>";
                message += string.Format("TargetSite: {0}", ex.TargetSite != null ? ex.TargetSite.ToString() : "");
                message += "<br/>";
                message += "-----------------------------------------------------------";

                string subject = "Error";
                var _email = "BrightreeDownloader@medbill.com";
                var _dispName = "Brightree Downloader";
                MailMessage emailMessage = new MailMessage();
                emailMessage.To.Add(email);
                emailMessage.From = new MailAddress(_email, _dispName);
                emailMessage.Subject = subject;
                emailMessage.Body = message;
                emailMessage.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.EnableSsl = false;
                    smtp.Host = "smtp.medbill.com";
                    smtp.UseDefaultCredentials = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                    smtp.Send(emailMessage);
                }
            }
            catch (Exception e)
            {
                DateTime EndTime = DateTime.Now;
                using (SqlConnection connection = new SqlConnection("Data Source=SCLKW01DB;Initial Catalog=MedBiller;Integrated Security=True"))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO AutomationRun_Log VALUES(@task, @completed, @date, @emailed)", connection))
                    {
                        command.Parameters.Add(new SqlParameter("task", "Downloader Failed Sending The Email - Exception " + ex.Message));
                        command.Parameters.Add(new SqlParameter("completed", false));
                        command.Parameters.Add(new SqlParameter("date", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("emailed", false));
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
        }
        public static void SendCustomEmailAsync(string customMessage, bool debug)
        {
            try
            {
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += "<br/>";
                message += "-----------------------------------------------------------";
                message += "<br/>";
                message += string.Format("Message: {0}", customMessage);
                message += "<br/>";
                message += "-----------------------------------------------------------";

                string subject = "Error";
                var _email = "BrightreeDownloader@medbill.com";
                var _dispName = "Brightree Downloader";
                MailMessage emailMessage = new MailMessage();
                emailMessage.To.Add(email);
                emailMessage.From = new MailAddress(_email, _dispName);
                emailMessage.Subject = subject;
                emailMessage.Body = message;
                emailMessage.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.EnableSsl = false;
                    smtp.Host = "smtp.medbill.com";
                    smtp.UseDefaultCredentials = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                    smtp.Send(emailMessage);
                }
            }
            catch (Exception e)
            {
                DateTime EndTime = DateTime.Now;
                using (SqlConnection connection = new SqlConnection("Data Source=SCLKW01DB;Initial Catalog=MedBiller;Integrated Security=True"))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO AutomationRun_Log VALUES(@task, @completed, @date, @emailed)", connection))
                    {
                        command.Parameters.Add(new SqlParameter("task", "Downloader Failed Sending The Email - Exception " + e.Message));
                        command.Parameters.Add(new SqlParameter("completed", false));
                        command.Parameters.Add(new SqlParameter("date", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("emailed", false));
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
        }
    }
}
