﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace SeleniumDownloaderCopyFIles
{
    class ErrorLogging
    {
        public static void log(string message)
        {
            Console.WriteLine(message);

            //var debug = Boolean.Parse(ConfigurationManager.AppSettings["debugMode"]);

            //if (debug)
            //{
            //    string path = "";
            //    if (client == null)
            //    {
            //        path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Log.txt";
            //    }
            //    else
            //    {
            //        path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + client.client + "Log.txt";
            //    }

            //    if (!File.Exists(path))
            //    {
            //        using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, 4096, true))
            //        using (StreamWriter sw = new StreamWriter(stream))
            //        {
            //            sw.WriteLine(message);
            //        }
            //    }
            //    else if (File.Exists(path))
            //    {
            //        using (FileStream stream = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.None, 4096, true))
            //        using (StreamWriter sw = new StreamWriter(stream))
            //        {
            //            sw.WriteLine(message);
            //        }
            //    }
            //}
        }

        public static void errorCatch(Exception ex, string message, bool completed)
        {
            bool debug = Boolean.Parse(ConfigurationManager.AppSettings["debugMode"]);
            if (ex != null)
            {
                using (SqlConnection connection = new SqlConnection("Data Source=SCLKW01DB;Initial Catalog=MedBiller;Integrated Security=True"))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO AutomationRun_Log VALUES(@task, @completed, @date, @emailed)", connection))
                    {
                        command.Parameters.Add(new SqlParameter("task", message));
                        command.Parameters.Add(new SqlParameter("completed", completed));
                        command.Parameters.Add(new SqlParameter("date", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("emailed", true));
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }

                if (!completed)
                {
                    Email.SendEmailAsync(ex, debug);
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection("Data Source=SCLKW01DB;Initial Catalog=MedBiller;Integrated Security=True"))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO AutomationRun_Log VALUES(@task, @completed, @date, @emailed)", connection))
                    {
                        command.Parameters.Add(new SqlParameter("task", message));
                        command.Parameters.Add(new SqlParameter("completed", completed));
                        command.Parameters.Add(new SqlParameter("date", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("emailed", true));
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }

                if (!completed)
                {
                    Email.SendCustomEmailAsync(message, debug);
                }
            }
        }
    }
}
